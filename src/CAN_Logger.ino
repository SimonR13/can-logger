// BEGIN CAN BUS
#include <SPI.h>
#include "mcp_can.h"

const int SPI_CS_PIN = 10;
MCP_CAN CAN(SPI_CS_PIN);

unsigned char flagRecv = 0;
unsigned char len = 0;
unsigned char buf[8];
char str[20];
// END CAN BUS

// BEGIN TFT
#include <SPFD5408_Adafruit_GFX.h>    // Core graphics library
#include <SPFD5408_Adafruit_TFTLCD.h> // Hardware-specific library
#include <SPFD5408_TouchScreen.h>     // Touch library

// Calibrates value
#define SENSIBILITY 300
#define MINPRESSURE 10
#define MAXPRESSURE 1000

//These are the pins for the shield!
#define YP A1
#define XM A2
#define YM 7
#define XP 6

short TS_MINX=150;
short TS_MINY=120;
short TS_MAXX=920;
short TS_MAXY=940;

// Init TouchScreen:
TouchScreen ts = TouchScreen(XP, YP, XM, YM, SENSIBILITY);

// LCD Pin
#define LCD_CS A3
#define LCD_CD A2
#define LCD_WR A1
#define LCD_RD A0
#define LCD_RESET A4 // Optional : otherwise connect to Arduino's reset pin

// Assign human-readable names to some common 16-bit color values:
#define BLACK   0x0000
#define BLUE    0x001F
#define RED     0xF800
#define GREEN   0x07E0
#define CYAN    0x07FF
#define MAGENTA 0xF81F
#define YELLOW  0xFFE0
#define WHITE   0xFFFF

// Init LCD
Adafruit_TFTLCD tft(LCD_CS, LCD_CD, LCD_WR, LCD_RD, LCD_RESET);

// Dimensions
uint16_t width = 0;
uint16_t height = 0;

// Buttons
#define BUTTONS 5
#define BUTTON_HOME 0
#define BUTTON_BOOKMARK 1
#define BUTTON_MSG 2
#define BUTTON_PIC 3
#define BUTTON_VID 4
// END TFT


void setup() {
  // init Serial
  Serial.begin(115200);
  Serial.println("CAN Logger Initialized!");

  // init TFT
  tft.reset();
  tft.begin(0x9341);
  tft.setRotation(2);
  width = tft.width() -1;
  height = tft.height() -1;

  tft.fillScreen(WHITE);

  // init screen
  tft.setCursor (15, 15);
  tft.setTextSize (1);
  tft.setTextColor(BLACK);
  tft.println("TFT Initialized");
  tft.print("TFT size is ");
  tft.print(tft.width());
  tft.print("x");
  tft.println(tft.height());

  tft.setCursor(20, 40);
  tft.setTextSize(3);
  tft.setTextColor(RED);
  tft.println("CAN Logger");
  tft.setCursor(65, 70);
  tft.setTextColor(BLUE);
  tft.println("v0.1");
  tft.setTextSize(2);
  tft.setTextColor(BLACK);
  tft.println("Init CAN-Bus");

  // Init CAN Bus
  tft.setCursor(15, 120);
  tft.setTextSize(1);
  tft.println("init speed with 100k");

  int counter = 0;
  int status = -1;
  while ((CAN_OK != (status = CAN.begin(CAN_100KBPS, MCP_16MHz, MODE_LISTENONLY))) && counter < 10) {
    counter++;
    tft.println("Init failed");
    tft.println("trying again in 100ms");
    delay(100);
  }

  if (status == CAN_OK) {
    tft.println("CAN BUS Shield init ok!");
    // tft.println("init filters");
    /*CAN.init_Filt(0, 0, 0x661);                          // there are 6 filter in mcp2515
    CAN.init_Filt(1, 0, 0x65F);                          // there are 6 filter in mcp2515

    CAN.init_Filt(2, 0, 0x3C0);                          // there are 6 filter in mcp2515
    CAN.init_Filt(3, 0, 0x3C1);                          // there are 6 filter in mcp2515

    CAN.init_Filt(4, 0, 0x4C8);
    CAN.init_Filt(5, 0, 0x623);

    tft.println("filters done");

    tft.println("init masks");
    CAN.init_Mask(0, 0, 0x7F0);                         // there are 2 mask in mcp2515, you need to set both of them
    CAN.init_Mask(1, 0, 0x7FF);
    tft.println("masks done");*/

    attachInterrupt(0, MCP2515_ISR, FALLING); // attach interrupt routine
  }
  else {
    tft.println("CAN Bus Shield init FAILED! EOL");
  }

  // tft.fillScreen(WHITE);
  tft.setTextColor(BLACK);
  tft.setTextSize(1);
  tft.setTextWrap(true);
}

void MCP2515_ISR() {
  flagRecv = 1;
}

char userInput;
unsigned char stmp[8] = {0, 1, 2, 3, 4, 5, 6, 7};
INT8U error = -1;
void loop() {
  if (tft.getCursorY() > tft.height()) {
    // clear screen
    tft.fillScreen(WHITE);
    tft.setCursor(15,15);
    tft.setTextSize (1);
    tft.setTextColor(BLACK);
  }
  // check if get data
  if(flagRecv) {
    // clear flag
    flagRecv = 0;
    // iterate over all pending messages
    // If either the bus is saturated or the MCU is busy,
    // both RX buffers may be in use and reading a single
    // message does not clear the IRQ conditon.
    while (CAN_MSGAVAIL == CAN.checkReceive()) {
      // tft.println("Msg available!");
      // read data,  len: data length, buf: data buf
      CAN.readMsgBuf(&len, buf);
      // tft.print("Msg for ID: ");
      Serial.printf("[%10ld][%03X][%2d]:\t", micros(), CAN.getCanId(), len);
      tft.println(CAN.getCanId());
      // Serial.println();
      // print the data
      for(int i = 0; i<len; i++) {
        Serial.printf("%02X ", buf[i]);
        // tft.print(buf[i]);
        // tft.print("\t");
      }
      tft.println();
      Serial.println();
    }
  }

  if ((error = CAN.checkError()) > 0) {
    if (error == 8) {
      tft.print("Receive Buffer Full! Interrupt cleared.");
    }
    else {
      tft.print("Error: ");
      tft.println(error);
    }
    error = -1;
  }

  while(Serial.available()) {
   userInput = Serial.read();
   Serial.print("userInput:");
   Serial.println(userInput);

   if (userInput=='1') {
     Serial.println("send dummy message");
     CAN.sendMsgBuf(0x04, 0, 8, stmp);
     delay(500);
    }
    else if (userInput=='2') {
     Serial.println(("send msg to 0x108"));
     stmp[0] = 0xE0;
     stmp[1] = 0x01;
     stmp[2] = 0x00;
     stmp[3] = 0x00;

     CAN.sendMsgBuf(0x108, 0, 8, stmp);
     delay(500);
    }
  }
}
